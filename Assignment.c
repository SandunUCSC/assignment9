#include <stdio.h>

int writefile();
int readfile();
int appendfile();

int main(){

writefile();
readfile();
appendfile();
return 0;
}

int writefile(){
FILE *fileWrite;

fileWrite=fopen("assignment9.txt","w");
fprintf(fileWrite,"UCSC is one of the leading institutes in Sri Lanka for computing studies.");
fclose(fileWrite);
return 0;
}

int readfile(){
FILE *fileRead;
char sentence[100];

fileRead=fopen("assignment9.txt","r");
while(!feof(fileRead))
{
fgets(sentence,100,fileRead);
puts(sentence);
}
fclose(fileRead);
return 0;
}

int appendfile(){
FILE *fileAppend;

fileAppend=fopen("assignment9.txt","a");
fprintf(fileAppend,"\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
fclose(fileAppend);
return 0;
}

